function [feature]=GetAllPicTextureNetworkFeature(picData,rNum,Tini,Tinc,Tf)
h=size(picData,1);
l=size(picData,2);
num=int16((((Tf-Tini)/Tinc)+1)*4);

feature=zeros(h,num);


for i=1:h
    disp(num2str(i));
    sum=0;
    for j=1:l
    pic=picData{i,j};
    rawFeature=GetTextureNetworkFeature2(pic,rNum,Tini,Tinc,Tf);
    sum=sum+rawFeature;

    

    end
    feature(i,:)=sum/5;

    
   
end