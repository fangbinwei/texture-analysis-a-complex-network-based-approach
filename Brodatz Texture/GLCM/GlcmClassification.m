clc;
clear;
load('BrodatzTextureTrain.mat');
load('BrodatzTextureTest.mat');


%%  normalize
data=trainData;
 paraNum=size(data,2)-1;

minFirst=zeros(1,paraNum);
maxFirst=zeros(1,paraNum);
for i=1:paraNum
    minFirst(1,i)=inf;
    maxFirst(1,i)=-inf;
end

for num=1:paraNum
data2=data(:,num)';
for i=1:size(data,1)
    if (data2(1,i)<minFirst(1,num))
        minFirst(1,num)=data2(1,i);
    end
    if(data2(1,i)>maxFirst(1,num))
        maxFirst(1,num)=data2(1,i);
    end
end

    for j=1:size(data,1)
        data(j,num)=(data(j,num)-minFirst(1,num))/(maxFirst(1,num)-minFirst(1,num));
    end
end


for num=1:paraNum

    for j=1:size(testData,1)
        testData(j,num)=(testData(j,num)-minFirst(1,num))/(maxFirst(1,num)-minFirst(1,num));
    end
end
%%


trainData=data;

 

%% 
% [bestacc,bestc,bestg]=SVMcgForClass(trainData(:,paraNum+1),trainData(:,1:paraNum),-10,10,-10,10,4);


var1=['-c ',num2str(512),' -g ',num2str(0.25),' -b 1'];
model=svmtrain(trainData(:,paraNum+1),trainData(:,1:paraNum),var1);

[predictlable,accuracy,decison_values]=svmpredict(testData(:,paraNum+1),testData(:,1:paraNum),model,'-b 1');