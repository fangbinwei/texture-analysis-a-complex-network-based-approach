clc;
clear;
load('BrodatzPic.mat');
trainData=[];
trainDataNum=0;
for classNum=1:112
    classNum
    if classNum==43||classNum==44||classNum==45
        continue
    end
    for picNum=1:20
        trainDataNum=trainDataNum+1;
        pic=Brodatz{classNum,picNum};
        glcm=graycomatrix(pic,'Offset',[0 1;0 4;-1 1;-4 4;-1 0;-4 0;-1 -1;-4 -4]);
        stats=graycoprops(glcm,'all');
        a1=stats.Contrast;
        a2=stats.Correlation;
        a3=stats.Energy;
        a4=stats.Homogeneity;
        trainData(trainDataNum,:)=[a1,a2,a3,a4,classNum];
    end
end


testDataNum=0;
testData=[];
for classNum=1:112
    classNum
    if classNum==43||classNum==44||classNum==45
        continue
    end
    for picNum=21:25
        testDataNum=testDataNum+1;
        pic=Brodatz{classNum,picNum};
        glcm=graycomatrix(pic,'Offset',[0 1;0 4;-1 1;-4 4;-1 0;-4 0;-1 -1;-4 -4]);
        stats=graycoprops(glcm,'all');
        a1=stats.Contrast;
        a2=stats.Correlation;
        a3=stats.Energy;
        a4=stats.Homogeneity;
        testData(testDataNum,:)=[a1,a2,a3,a4,classNum];
    end
end

% save('BrodatzTextureTrain.mat','trainData');
% save('BrodatzTextureTest.mat','testData');

