clc;
clear;
load('BrodatzPic.mat');
for classNum=2:2
    classNum
    if classNum==43||classNum==44||classNum==45
        continue
    end
    for picNum=1:1
        
        pic=Brodatz{classNum,picNum};
        I=pic;
        figure,subplot(1,5,1),imshow(I);
        [T1,SM1]=graythresh(I);
 I2=im2bw(I,T1);
subplot(1,5,2),imshow(I2);

hx=[-1 -2 -1;0 0 0 ;1 2 1];%生产sobel垂直梯度模板  
 hy=hx';                             %生产sobel水平梯度模板  
   
 gradx=filter2(hx,I,'same');  
 gradx=abs(gradx); %计算图像的sobel垂直梯度
 
  grady=filter2(hy,I,'same');  
 grady=abs(grady); %计算图像的sobel水平梯度  
   grad=gradx+grady;%得到图像的sobel梯度
     min1=min(grad(:));
  max1=max(grad(:));
    A=grad;

  [m,n]=size(grad);
  for i=1:m
    for j=1:n

        A(i,j)=((A(i,j)-min1)/(max1-min1));

        
    end
  end
A_1=im2uint8(A);
subplot(1,5,3),imshow(A_1);
 [T,SM]=graythresh(A_1);
 A_2=im2bw(A_1,T);
subplot(1,5,4),imshow(A_2);
[y,x]=find(A_2==1);

% se=strel('disk',1);
%  A_3=imdilate(A_2,se);%腐蚀
%  subplot(1,5,5),imshow(A_3);
    end
end

