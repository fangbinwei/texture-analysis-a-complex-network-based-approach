clc;
clear;
load('BrodatzPic.mat');
trainData=[];
trainDataNum=0;
for classNum=1:112
    classNum
    if classNum==43||classNum==44||classNum==45
        continue
    end
    for picNum=1:20
        trainDataNum=trainDataNum+1;
        pic=Brodatz{classNum,picNum};
        [y_,x_]=GetSobel(pic);
        feature=GetBrodatzSobelNetwork(pic,y_,x_,3,0.005,0.015,0.53);
        trainData(trainDataNum,:)=[feature,classNum];
    end
end


testDataNum=0;
testData=[];
for classNum=1:112
    classNum
    if classNum==43||classNum==44||classNum==45
        continue
    end
    for picNum=21:25
        testDataNum=testDataNum+1;
        pic=Brodatz{classNum,picNum};
        [y_,x_]=GetSobel(pic);
        feature=GetBrodatzSobelNetwork(pic,y_,x_,3,0.005,0.015,0.53);
        testData(testDataNum,:)=[feature,classNum];
    end
end