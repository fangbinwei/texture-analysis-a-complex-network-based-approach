function [y,x]=GetSobel(picData)
I=picData;
hx=[-1 -2 -1;0 0 0 ;1 2 1];%生产sobel垂直梯度模板  
hy=hx';                             %生产sobel水平梯度模板  
   
gradx=filter2(hx,I,'same');  
gradx=abs(gradx); %计算图像的sobel垂直梯度
 
grady=filter2(hy,I,'same');  
grady=abs(grady); %计算图像的sobel水平梯度  
grad=gradx+grady;%得到图像的sobel梯度
min1=min(grad(:));
max1=max(grad(:));
A=grad;

[m,n]=size(grad);
for i=1:m
   for j=1:n

        A(i,j)=((A(i,j)-min1)/(max1-min1));

        
   end
end
A_1=im2uint8(A);
[T,SM]=graythresh(A_1);
A_2=im2bw(A_1,T);
[y,x]=find(A_2==1);