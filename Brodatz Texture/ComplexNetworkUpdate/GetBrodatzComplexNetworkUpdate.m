
function [feature]=GetBrodatzComplexNetworkUpdate(image)

% image=rgb2gray(imread('luowen.jpg'));
% image=imresize(image,[50 50]);
% image=medfilt2(image);
image=im2double(image)*255;
% [~,image]=graycomatrix(image,'NumLevels',255); 
level=255;
L=level;
% image=imresize(image,[180 150]);
% image=[44,31,31,29,35,103,39,29,31;
%     25,23,27,21,42,91,56,20,32;
%     28,18,21,37,69,56,49,21,34;
%     82,20,52,140,70,40,44,30,33;
%     113,17,45,155,52,44,50,35,31;
%     95,20,12,20,58,129,26,32,36;
%     72,28,28,14,60,52,39,34,35;
%     38,15,13,17,53,62,40,27,37;
%     18,16,14,10,38,65,39,26,35;];
% figure,
% imshow(image);
[y,x]=size(image);
%r确定non-directed edge
r=2;

t=0.27;



degree=[];
degreeCount=0;
maxDegree=-inf;
for verticeI=1:y
    
    for verticeJ=1:x
%         if image(verticeI,verticeJ)<100
%             continue
%         end
       
    
        weightCount=0;

        for i=1:y
            if ((i-verticeI)*(i-verticeI))>(r*r)
                continue
            end
            
            for j=1:x
                if ((j-verticeJ)*(j-verticeJ))>(r*r)
                    continue
                end
                distance=(i-verticeI)*(i-verticeI)+(j-verticeJ)*(j-verticeJ);
%                 if image(i,j)<100
%                     edgeFlag=1;
%                 end
                if (distance<=(r*r))&&(distance~=0)
                    weight=(distance+r*r*abs(image(verticeI,verticeJ)-image(i,j))/L)/(r*r*2);
                    if weight<t
                        weightCount=weightCount+1;
                        
                     
%                         if degreeCount<other

%                         network=[network;degreeCount,other,weight];
%                         end
                    end
                    
                end
            end
        end
        if weightCount<7
            degreeCount=degreeCount+1;
            degree(degreeCount,1)=weightCount;
            if (degree(degreeCount,1)>maxDegree)
                 maxDegree=degree(degreeCount,1);
            end
         
        end
        
        
    end
end


hCount=0;
hSum=0;
h=zeros(maxDegree,1);
p=zeros(maxDegree,1);
for i=1:maxDegree
    hCount=0;
    for j=1:degreeCount
        if degree(j,1)==i
            hCount=hCount+1;
        end
    end
    h(i,1)=hCount;
    hSum=hSum+hCount;
end

for i=1:maxDegree
    p(i,1)=h(i,1)/hSum;
end


%% 计算特征值
mean=0;
for i=1:maxDegree
    mean=mean+i*p(i,1);
end

contrast=0;
for i=1:maxDegree
    contrast=contrast+i*i*p(i,1);
end

energy=0;
for i=1:maxDegree
    energy=energy+p(i,1)*p(i,1);
end

entropy=0;
for i=1:maxDegree
    
    if p(i,1)~=0
        entropy=entropy+p(i,1)*log2(p(i,1));
    end 
    
end
entropy=-entropy;

feature=[mean,contrast,energy,entropy];
