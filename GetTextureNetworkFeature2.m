function [feature]=GetTextureNetworkFeature2(picData,rNum,Tini,Tinc,Tf)
% tic;
image=rgb2gray(picData);
image=im2double(image)*255;

[y,x]=size(image);

%r确定non-directed edge
r=rNum;
L=255;
num=int16(((Tf-Tini)/Tinc)+1);

degree=zeros(x*y,num);
degreeCount=0;
maxDegree=zeros(1,num);
for w=1:num
    
    maxDegree(1,w)=-inf;
end

for verticeI=1:y
    for verticeJ=1:x
        degreeCount=degreeCount+1;
        
        for i=1:y
            if ((i-verticeI)*(i-verticeI))>(r*r)
                continue
            end
            
            for j=1:x
                if ((j-verticeJ)*(j-verticeJ))>(r*r)
                    continue
                end
                distance=(i-verticeI)*(i-verticeI)+(j-verticeJ)*(j-verticeJ);
                if (distance<=(r*r))&&(distance~=0)
                    weight=(distance+r*r*abs(image(verticeI,verticeJ)-image(i,j))/L)/(r*r*2);
                        tCount=0;
                        for t=Tini:Tinc:Tf
                            tCount=tCount+1;
                            if weight<t
                                degree(degreeCount,tCount)=degree(degreeCount,tCount)+1;
                            end
                        end
                   
                    
                end
            end
        end
        

    for w=1:num
        if (degree(degreeCount,w)>maxDegree(1,w))
            maxDegree(1,w)=degree(degreeCount,w);
        end
        
    end
        
    end
end


feature=zeros(1,4);
for w=1:num
    hSum=0;
    h=zeros(maxDegree(1,w),1);
    p=zeros(maxDegree(1,w),1);
    for i=1:maxDegree(1,w)
        hCount=0;
        for j=1:degreeCount
            if degree(j,w)==i
                hCount=hCount+1;
            end
        end
        h(i,1)=hCount;
        hSum=hSum+hCount;
    end

    for i=1:maxDegree(1,w)
        p(i,1)=h(i,1)/hSum;
    end


    %% 计算特征值
    mean=0;
    for i=1:maxDegree(1,w)
        mean=mean+i*p(i,1);
    end

    contrast=0;
    for i=1:maxDegree(1,w)
        contrast=contrast+i*i*p(i,1);
    end

    energy=0;
    for i=1:maxDegree(1,w)
        energy=energy+p(i,1)*p(i,1);
    end

    entropy=0;
    for i=1:maxDegree(1,w)

        if p(i,1)~=0
            entropy=entropy+p(i,1)*log2(p(i,1));
        end 

    end
    entropy=-entropy;
    feature=[feature,mean,contrast,energy,entropy];
    
end

feature=feature(5:end);

% disp('complete');
% toc;
