function [feature]=GetTextureNetworkFeature(picData,rNum,threshold)
% tic;
image=rgb2gray(picData);
image=im2double(image)*255;

[y,x]=size(image);
graph=cell(y,x);
%r确定non-directed edge
r=rNum;
L=256;
t=threshold;
degree=zeros(x*y,1);
degreeCount=0;
minDegree=inf;
maxDegree=-inf;
for verticeI=1:y
    for verticeJ=1:x
        degreeCount=degreeCount+1;
        nodeGroup=zeros(1,3);
        for i=1:y
            if ((i-verticeI)*(i-verticeI))>(r*r)
                continue
            end
            
            for j=1:x
                if ((j-verticeJ)*(j-verticeJ))>(r*r)
                    continue
                end
                distance=(i-verticeI)*(i-verticeI)+(j-verticeJ)*(j-verticeJ);
                if (distance<=(r*r))&&(distance~=0)
                    weight=(distance+r*r*abs(image(verticeI,verticeJ)-image(i,j))/L)/(r*r*2);
                    if weight<t
                        nodeGroup=[nodeGroup;image(verticeI,verticeJ),image(i,j),weight];
                    end
                    
                end
            end
        end
        nodeGroup=nodeGroup(2:size(nodeGroup,1),:);
        graph{verticeI,verticeJ}=nodeGroup;
        degree(degreeCount,1)=size(nodeGroup,1);
        if (degree(degreeCount,1)<minDegree)
            minDegree=degree(degreeCount,1);
        end
        if (degree(degreeCount,1)>maxDegree)
            maxDegree=degree(degreeCount,1);
        end
    end
end

hSum=0;
h=zeros(maxDegree,1);
p=zeros(maxDegree,1);
for i=1:maxDegree
    hCount=0;
    for j=1:degreeCount
        if degree(j,1)==i
            hCount=hCount+1;
        end
    end
    h(i,1)=hCount;
    hSum=hSum+hCount;
end

for i=1:maxDegree
    p(i,1)=h(i,1)/hSum;
end


%% 计算特征值
mean=0;
for i=1:maxDegree
    mean=mean+i*p(i,1);
end

contrast=0;
for i=1:maxDegree
    contrast=contrast+i*i*p(i,1);
end

energy=0;
for i=1:maxDegree
    energy=energy+p(i,1)*p(i,1);
end

entropy=0;
for i=1:maxDegree
    
    if p(i,1)~=0
        entropy=entropy+p(i,1)*log2(p(i,1));
    end 
    
end
entropy=-entropy;
feature=[mean,contrast,energy,entropy];
% disp('complete');
% toc;
