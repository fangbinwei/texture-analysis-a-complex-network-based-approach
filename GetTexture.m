clc;
clear;
load('250差米形.mat');
load('250短螺纹.mat');
load('250二档冲头.mat');
load('250高档颗头螺纹.mat');
load('250高档米珠.mat');
load('250高档面光.mat');
load('250中档米珠.mat');
load('250四面光.mat');

chamixingGlcm250=GetAllPicTextureNetworkFeature(chamixing,2,0.14,0.015,0.53);
disp('1');
duanluowenGlcm250=GetAllPicTextureNetworkFeature(duanluowen,2,0.14,0.015,0.53);
disp('2');
erdangchongtouGlcm250=GetAllPicTextureNetworkFeature(erdangchongtou,2,0.14,0.015,0.53);
disp('3');
gaodangketouluowenGlcm250=GetAllPicTextureNetworkFeature(gaodangketouluowen,2,0.14,0.015,0.53);
disp('4');
gaodangmizhuGlcm250=GetAllPicTextureNetworkFeature(gaodangmizhu,2,0.14,0.015,0.53);
disp('5');
gaodangmianguangGlcm250=GetAllPicTextureNetworkFeature(gaodangmianguang,2,0.14,0.015,0.53);
disp('6');
zhongdangmizhuGlcm250=GetAllPicTextureNetworkFeature(zhongdangmizhu,2,0.14,0.015,0.53);
disp('7');
simianguangGlcm250=GetAllPicTextureNetworkFeature(simianguang,2,0.14,0.015,0.53);
disp('8');

save('250差米形TextureNetwork5pic2_014_0015_053.mat','chamixingGlcm250');
save('250短螺纹TextureNetwork5pic2_014_0015_053.mat','duanluowenGlcm250');
save('250二档冲头TextureNetwork5pic2_014_0015_053.mat','erdangchongtouGlcm250');
save('250高档颗头螺纹TextureNetwork5pic2_014_0015_053.mat','gaodangketouluowenGlcm250');
save('250高档面光TextureNetwork5pic2_014_0015_053.mat','gaodangmianguangGlcm250');
save('250高档米珠TextureNetwork5pic2_014_0015_053.mat','gaodangmizhuGlcm250');
save('250四面光TextureNetwork5pic2_014_0015_053.mat','simianguangGlcm250');
save('250中档米珠TextureNetwork5pic2_014_0015_053.mat','zhongdangmizhuGlcm250');