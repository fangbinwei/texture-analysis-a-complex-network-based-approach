clc;
clear;
% close all;


tic;

image=rgb2gray(imread('luowen.jpg'));
image=imresize(image,[50 50]);
% image=medfilt2(image);
image=im2double(image)*255;
% [~,image]=graycomatrix(image,'NumLevels',255); 
level=255;
L=level;
% image=imresize(image,[180 150]);
% image=[44,31,31,29,35,103,39,29,31;
%     25,23,27,21,42,91,56,20,32;
%     28,18,21,37,69,56,49,21,34;
%     82,20,52,140,70,40,44,30,33;
%     113,17,45,155,52,44,50,35,31;
%     95,20,12,20,58,129,26,32,36;
%     72,28,28,14,60,52,39,34,35;
%     38,15,13,17,53,62,40,27,37;
%     18,16,14,10,38,65,39,26,35;];
% figure,
% imshow(image);
[y,x]=size(image);
%r确定non-directed edge
r=2;

t=0.27;


graph=cell(y,x);
network=0;
network2=zeros(1,8);

networkCount=0;
for verticeI=1:y
    verticeI
    for verticeJ=1:x
        if image(verticeI,verticeJ)<100
            continue
        end
       
        networkTemp=0;
        weightCount=0;
        networkTempCount=0;
        edgeFlag=0;
        for i=1:y
            if ((i-verticeI)*(i-verticeI))>(r*r)
                continue
            end
            
            for j=1:x
                if ((j-verticeJ)*(j-verticeJ))>(r*r)
                    continue
                end
                distance=(i-verticeI)*(i-verticeI)+(j-verticeJ)*(j-verticeJ);
                if image(i,j)<100
                    edgeFlag=1;
                end
                if (distance<=(r*r))&&(distance~=0)
                    weight=(distance+r*r*abs(image(verticeI,verticeJ)-image(i,j))/L)/(r*r*2);
                    if weight<t
                        weightCount=weightCount+1;
                        networkCount=networkCount+1;
                        networkTempCount=networkTempCount+1;
                        
                        I=(verticeI-1)*x+verticeJ;
                        other=(i-1)*x+j;
%                         if degreeCount<other
                        networkTemp(networkTempCount,1)=I;
                        networkTemp(networkTempCount,2)=other;
                        networkTemp(networkTempCount,3)=weight;
                        networkTemp(networkTempCount,4)=verticeJ;
                        networkTemp(networkTempCount,5)=verticeI;
                        networkTemp(networkTempCount,7)=j;
                        networkTemp(networkTempCount,8)=i;
                        
                        network(networkCount,1)=I;
                        network(networkCount,2)=other;
                        network(networkCount,3)=weight;
                        network(networkCount,4)=verticeJ;
                        network(networkCount,5)=verticeI;
                        network(networkCount,7)=j;
                        network(networkCount,8)=i;
%                         network=[network;degreeCount,other,weight];
%                         end
                    end
                    
                end
            end
        end
        if weightCount<7&&edgeFlag~=1
            hang=size(network2,1);
            network2((hang+1):(weightCount+hang),1:8)=networkTemp;
        end
        
        
    end
end

image2=image;
for i=1:size(network2,1)
    
        row=network2(i,5);
        col=network2(i,4);
        if row~=0||col~=0
            image2(row,col)=0;
        end
        row=network2(i,8);
        col=network2(i,7);
        if row~=0||col~=0
            image2(row,col)=0;
        end
end
figure,
subplot(1,2,1),imshow(image,[]);
subplot(1,2,2),imshow(image2,[]);
% network=network(2:size(network,1),:);


% hCount=0;
% hSum=0;
% h=zeros(maxDegree,1);
% p=zeros(maxDegree,1);
% for i=1:maxDegree
%     hCount=0;
%     for j=1:degreeCount
%         if degree(j,1)==i
%             hCount=hCount+1;
%         end
%     end
%     h(i,1)=hCount;
%     hSum=hSum+hCount;
% end
% 
% for i=1:maxDegree
%     p(i,1)=h(i,1)/hSum;
% end
% 
% 
% %% 计算特征值
% mean=0;
% for i=1:maxDegree
%     mean=mean+i*p(i,1);
% end
% 
% contrast=0;
% for i=1:maxDegree
%     contrast=contrast+i*i*p(i,1);
% end
% 
% energy=0;
% for i=1:maxDegree
%     energy=energy+p(i,1)*p(i,1);
% end
% 
% entropy=0;
% for i=1:maxDegree
%     
%     if p(i,1)~=0
%         entropy=entropy+p(i,1)*log2(p(i,1));
%     end 
%     
% end
% entropy=-entropy;
toc;
